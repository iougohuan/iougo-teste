/*global iougoTeste, $*/


window.iougoTeste = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
        'use strict';

        // cria uma colecao
        var colection = new iougoTeste.Collections.BlogCollection();

        // quando voce adicionar algo na colecao
        // mostre no .html
        colection.on('add', function(model){

            var v = new iougoTeste.Views.BlogView({
                model : model
            });

            v.render();

            $('#result').append( v.$el.html() );

        });

        // adicione na colecao, para testar, deve mostrar no html
        colection.add(new iougoTeste.Models.BlogModel({
            name : 'Testando o backbone',
            description : 'Testando o backbone, description'
        }));

        // coloca o evento no form para que quando der submit,
        // crie um modelo e adiciona na colecao
        $('form').on('submit', function(e){

            e.preventDefault(); 

            colection.add(new iougoTeste.Models.BlogModel({
                name : $('#name').val(),
                description : $('#description').val()
            }));

            this.reset();
        });

    }
};

$(document).ready(function () {
    'use strict';
    iougoTeste.init();
});
