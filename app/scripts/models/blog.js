/*global iougoTeste, Backbone*/

iougoTeste.Models = iougoTeste.Models || {};

(function () {
    'use strict';

    iougoTeste.Models.BlogModel = Backbone.Model.extend({

        url: '',

        initialize: function() {
        },

        defaults: {
            name : '',
            description : ''
        },

        validate: function(attrs, options) {
        },

        parse: function(response, options)  {
            return response;
        }
    });

})();
