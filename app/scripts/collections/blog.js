/*global iougoTeste, Backbone*/

iougoTeste.Collections = iougoTeste.Collections || {};

(function () {
    'use strict';

    iougoTeste.Collections.BlogCollection = Backbone.Collection.extend({

        model: iougoTeste.Models.BlogModel

    });

})();
